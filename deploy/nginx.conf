server {
    listen 80 default_server;
    return 444;
}

server {
    listen 80;
    server_name open-store.io www.open-store.io;
    return 301 https://open-store.io$request_uri;
}

server {
    listen 443 ssl;
    server_name open-store.io;

    if ($host !~* ^open-store\.io$ ) {
        return 444;
    }

    ssl on;
    ssl_certificate /etc/letsencrypt/live/open-store.io/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/open-store.io/privkey.pem;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

    gzip on;
    gzip_min_length 500;
    gzip_types text/plain text/html application/json application/javascript text/css;

    client_max_body_size 150M;

    location / {
        proxy_pass http://localhost:8080/;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ \.(ratings|badges|banners|categories|css|fonts|img|index.html|js|logo.png|vanilla.css)$ {
        root /srv/openstore-web/current/dist/;
    }
}
